$(document).ready(function(){

    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:0,
        stagePadding:0,
        nav:true,
        dots: false,
        items:1,
        smartSpeed:800
    })

});