$(document).ready(function(){
    
    $('.navbar__burger').click(function() {
        $('.navbar__burger').toggleClass('navbar__burger-open');
        $('.navbar__menu').toggleClass('navbar__burger-open');
    });


    $(".carousel-courses__description").click(function(){
        $(this).find('.more-link').toggleClass('hide-link');
        $(this).find('.less-link').toggleClass('show-link');
        $(this).find('.carousel-courses__block-list').toggleClass('active-list');
    });

    
    $(".results__block").click(function(){
        $(this).find('.arrow').toggleClass('active');
        $(this).find('.toggler').toggleClass('results__block-listtoggle');
    });


    $('.questions__block').click(function(){
        $(this).find('.plus').toggleClass('minus');
        $(this).find('.questions__block-text').toggleClass('questions__block-showless');
        $(this).toggleClass('questions__block-active')
    });


    $('.popular-courses__badge').click(function(){
        let target = $(this).attr("aim") // смотрим  цель бейджа

        if($(this).hasClass('active-badge')){
            // если уже бадж активен, то сбрасываем всё
            $(document).find('.popular-courses__badge').removeClass('active-badge')
            $(document).find('.popular-courses__block').removeClass('isDisplayFlex');
        }else{
            // если бадж не активен, то чистим все и добавляем классы по таргету
            $(document).find('.popular-courses__badge').removeClass('active-badge')
            $(document).find('.popular-courses__block').removeClass('isDisplayFlex')

            $(this).addClass('active-badge');
            $(document).find(target).toggleClass('isDisplayFlex'); // Показываем нужные бэйджи
        }

        let badgesIsActive = $(document).find('.active-badge')?.length // смотрим есть ли активные бейджи


        // если есть бэйджи активные, то скрываем всех детей
        badgesIsActive ?
            $(document).find('.popular-courses__courses').addClass('hide-children') :
            $(document).find('.popular-courses__courses').removeClass('hide-children')

    });



    $(".scrollToCarousel").click(function() { // куда кликаем
        let aimSlide =  $(this).attr("aim") // смотрим какой слайд в атрибутах

        $('html, body').animate({
            scrollTop: $('.carousel-courses__title').offset().top // куда едем
        }, 1000); // с какой скоростью

        $('#owlCarousel').trigger('to.owl.carousel', parseInt(aimSlide) -1 ) // какой слайд активируем на каруселе. - 1 так чтобы он был первым слева
    });



    if ( document.documentElement.clientWidth > 991 ){
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:30,
        stagePadding:20,
        nav:true,
        dots: false,

        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:2
            }
        }
    })}


});