$(document).ready(function(){

    $('.owl-carousel').owlCarousel({
		dots:false,
        nav:true,
		smartSpeed: 600,
		autoplay:true,
		animateOut: 'slideOutDown',
		animateIn: 'flipInX',
		loop:true,
		center:true,
		autoplayHoverPause:true,
		responsive: {
			0: {
				items: 1,
				nav:true
			},
			430: {
				items: 3
			},
			767: {
				items: 3,
				nav:false
			},
			1280: {
				items: 5
			},
			3550: {
				items: 5
			}
		}
    })

	
	$('.scroll__item').on('click', function(e){
		e.preventDefault();
		$('.scroll__item').removeClass('scroll__item--active');
		$(this).addClass('scroll__item--active');
	})

    $('#avtory').on('click', function () {
		$('#contentElem').css({
			'transform': 'translateY(0)'
		});
	})
	$('#situation').on('click', function () {
		$('#contentElem').css({
			'transform': 'translateY(-500px)'
		});
	})
	$('#example').on('click', function () {
		$('#contentElem').css({
			'transform': 'translateY(-945px)'
		});
	})
	$('#zakon').on('click', function () {
		$('#contentElem').css({
			'transform': 'translateY(-2853px)'
		});
	})
	$('#pravovaya').on('click', function () {
		$('#contentElem').css({
			'transform': 'translateY(-3360px)'
		});
	})
	$('#special').on('click', function () {
		$('#contentElem').css({
			'transform': 'translateY(-3770px)'
		});
	})

});